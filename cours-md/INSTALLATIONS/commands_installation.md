# :small_orange_diamond: TYPESCRIPT

````
npm install -D typescript tsc
npx tsc init
````

# :small_orange_diamond: EXPRESS

````
npm install express body-parser cors
````

# :small_orange_diamond: SEQUELIZE

````
npm install --save sequelize
````   

- Driver MySQL

````
npm install --save mysql2
````

# :small_orange_diamond: MONGOOSE

````
npm install mongoose --save
````

# :small_orange_diamond: MONGODB

:link:**Site MoongoDB :** [Ici.](https://www.mongodb.com/fr)

*Docs > Server > Installation > Install MongoDB Community Edition*

:small_red_triangle_down: Terminal MacOS
````
xcode-select --install
````

* Installer Brew

````
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
````

* Vérifier que l'installation s'est bien déroulé

````
brew tap mongodb/brew
````

````
brew install mongodb-community@4.4
````

* Pour lancer MongoDB

````
brew services start mongodb-community@4.4
````

* Pour stopper MongoDB

````
brew services stop mongodb-community@4.4
````

* Vérifier que MongoDB est bien lancé

````
brew services list
````

* Vérifier la connexion

````
mongo
````

# :small_orange_diamond: ANGULAR

- Install the Angular CLI:

````
npm install -g @angular/cli
````

- Voir les commandes qu'on a:

````
ng
````

- Créer nouvelle application:

````
ng new my-app
````

- Accepter le routing avec:

````
y
````
Choisir ``SCSS``.

- Accéder à l'application:

```
cd my-app
```

### Bootstrap/Angular

```
npm install bootstrap
```
- Vérifier si bootstrap est bien installé.
- Ouvrir ``angular.json``.
- Dans styles: **Ajouter** :
````
"node_modules/bootstrap/dist/css/bootstrap.min.css"
````

# :small_orange_diamond: REACT

````   
npx create-react-app mon-app
````
````   
cd mon-app
````
````   
npm start
````
````   
npm run build
```` 

````
npm install
````

### ROUTER

````
npm install react-router-dom
````

# :small_orange_diamond: JSONWEBTOKEN

````
npm install jsonwebtoken
````

