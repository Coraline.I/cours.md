# REACT | API

### SOMMAIRE

1. [React - Axios http client](#1-react---axios-http-client)



## 1. React - Axios http client

:link: **Cours vidéo 1 :** [Ici.](https://youtu.be/9o_xq_bIHSw)
<br>:link: **Cours vidéo 2 :** [Ici.](https://youtu.be/h_Z7DnsGC5w)

La librairie **Axios** est basée sur les **Promesses**.
<br>Permet de réaliser du code à partir d'un certain moment lorsque la **promesse est résolue**.

### :white_small_square: Installation

````
npm i axios
````

### :white_small_square: Retour dans le projet

* Créer fichier **Users.js**
* Ne pas oublier d'insérer la Route dans **App.js**

````js
<Route path="/users" component={Users} />
````

:small_red_triangle_down: **Users.js**

````javascript
import {Component} from "react";

class Users extends Component {

    state = {
        users: []
    };
    

    render() {
        return (
            <>
                <div>User 1</div>
                <div>User 2</div>
            </>
        );

    }
}

export default Users;
````


### :white_small_square: Récupérer utilisateur de l'API

:small_red_triangle_down: **Users.js**

````javascript
import {Component} from "react";
import axios from "axios"; // 1. importer axios

class Users extends Component {
    
 // 2. Rajouter un state avec un tableau vide
    state = {
        users: []
    };

    componentDidMount() {
        // 3. Récupération des users
        const response = axios.get("http://localhost:8080/users").then((response) => {
            console.log("response :", response.data);
            this.setState({users: response.data});
        });

    }

    render() {
        const users =  this.state.users.map((user) => {
            return <div key={user.id}>{user.firstName}</div>
        });
        return (
            <>
                {user}
            </>
        );


    }
}

export default Users;
````

### :white_small_square: Supprimer l'utilisateur lorsqu'on click dessus

````javascript

    // Supprimer un utilisateur lorsqu'on clique dessus
    
    delete(id) {
        axios.delete("http://localhost:8080/users" + id).then(res => {
            console.log("delete:", response);
        });
    }

    render()
    { // Rajouter un click
        const users = this.state.users.map((user) => {
            return <div key={user.id} onClick={() => this.delete(user.id)}>{user.firstName}</div>
        });
        return (
            <>
                {user}
            </>
        );
    }
````

### :white_small_square: Mettre à jour le tableau d'utilisateur en supprimant le user supprimé

````javascript
delete(id) {
        axios.delete("http://localhost:8080/users" + id).then(res => {
            console.log("delete:", response);
            // Recopier les données du tableau avec le spread
            const users = [...this.state.users];
            const idx = users.findIndex((user) => user.id === id);
            users.splice(idx, 1);
            this.setState({users: [...users]});
        });
    }

````

[[Retour au Sommaire]](#sommaire)