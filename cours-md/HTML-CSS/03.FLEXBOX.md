## FLEXBOX

### SOMMAIRE

1. [Justify content](#1-justify-content)
2. [Align items](#2-align-items)
3. [Flex direction](#3-flex-direction)
4. [Order](#4-order)
5. [Align self](#5-align-self)
6. [Flex wrap](#6-flex-wrap)
7. [Flex flow](#7-flex-flow)
8. [Align content](#8-align-content)


````css
.box-ex {
display: flex;
}
````
[[Retour au Sommaire]](#sommaire)

### 1. Justify content

````
Justify-content aligne les éléments horizontalement.
````
* **flex-start** : Les éléments s'alignent au côté gauche du conteneur.
* **flex-end** : Les éléments s'alignent au côté droit du conteneur.
* **center** : Les éléments s'alignent au centre du conteneur.
* **space-between** : Les éléments s'affichent avec un espace égal entre eux.
* **space-around** : Les éléments s'affichent avec un espacement égal à l'entour d'eux.


[[Retour au Sommaire]](#sommaire)

### 2. Align items

````
Align-items aligne les éléments verticalement.
````

* **flex-start** : Les éléments s'alignent au haut du conteneur.
* **flex-end** : Les éléments s'alignent au bas du conteneur.
* **center** : Les éléments s'alignent au centre vertical du conteneur.
* **baseline** : Les éléments s'alignent à la ligne de base du conteneur.
* **stretch** : Les éléments sont étirés pour s'adapter au conteneur.


[[Retour au Sommaire]](#sommaire)

### 3. Flex direction

````
Flex-direction définit la direction dans laquelle les éléments sont placés dans le conteneur.
````

* **row** : Les éléments sont disposés dans la même direction que le texte.
* **row-reverse** : Les éléments sont disposés dans la direction opposée au texte.
* **column** : Les éléments sont disposés de haut en bas.
* **column-reverse** : Les éléments sont disposés de bas en haut.
  
[[Retour au Sommaire]](#sommaire)
  
### 4. Order

Parfois, inverser l'ordre de la rangée ou la colonne ne suffit pas. 
<br>Dans ces cas, on peut appliquer la propriété **order** à des éléments individuels. 
<br>Par défaut, les éléments ont une valeur de 0, mais on peut utiliser cette propriété pour changer la valeur à un entier positif ou négatif.

````css
.box-ex {
order: 1;
}
````

[[Retour au Sommaire]](#sommaire)

### 5. Align self

````
Align-self accepte les mêmes valeurs que align-items, mais s'applique seulement à l'élément ciblé.
````

[[Retour au Sommaire]](#sommaire)

### 6. Flex wrap

````
Flex-wrap
````

* **nowrap** : Tous les éléments sont tenus sur une seule ligne.
* **wrap** : Les éléments s'enveloppent sur plusieurs lignes au besoin.
* **wrap-reverse** : Les éléments s'enveloppent sur plusieurs lignes dans l'ordre inversé.

[[Retour au Sommaire]](#sommaire)

### 7. Flex flow

Les deux propriétés flex-direction et flex-wrap sont utilisées tellement souvent ensembles que le raccourci flex-flow a été créé pour les combiner. 
<br>Ce raccourci accepte les valeurs des deux propriétés séparées par un espace.

````javascript
EX: flex-flow: row wrap // pour configurer les colonnes et les faire s'envelopper.
````

[[Retour au Sommaire]](#sommaire)

### 8. Align content

````
Align-content définit comment plusieurs lignes sont espacées de l'une à l'autre.
````

* **flex-start** : Les lignes sont amassées dans le haut du conteneur.
* **flex-end**: Les lignes sont amassées dans le bas du conteneur.
* **center** : Les lignes sont amassées dans le centre vertical du conteneur.
* **space-between** : Les lignes s'affichent avec un espace égal entre eux.
* **space-around** : Les lignes s'affichent avec un espace égal autour d'eux.
* **stretch** : Les lignes sont étirées pour s'adapter au conteneur.

**Align-content** détermine l'espace entre les lignes.
<br>**Align-items** détermine comment les éléments dans leur ensemble sont alignées à l'intérieur du conteneur. 

:black_small_square: Quand il n'y a qu'une seule ligne, align-content n'a aucun effet.

[[Retour au Sommaire]](#sommaire)