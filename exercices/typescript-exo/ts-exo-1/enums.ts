/*
    Enums :

 */

// CORRECTION: https://youtu.be/JO0IqnJ10mQ

// Déclarez un énumérateur nommé "Mode" avec les propriétés "EASE", "NORMAL" et "HARD"

enum Mode {
    EASE,
    NORMAL,
    HARD
}

// Typez la variable "mode" avec l'énumérateur "Mode";
let mode: Mode;

// Assignez à la variable "mode" le mode "EASE"

mode = Mode.EASE;

// Assignez à la variable "mode" le mode "NORMAL"

mode = Mode.NORMAL;

// Assignez à la variable "mode" le mode "HARD"

mode = Mode.HARD;

// Créez une condition qui vérifie :
// SI la variable mode est strictement égale à la valeur du mode "HARD"
// ALORS j'affiche dans la console "mode difficile"
// SiNON SI la variable mode est strictement égale à la valeur du mode "NORMAL"
// ALORS j'affiche dans la console "mode normale"
// SiNON SI la variable mode est strictement égale à la valeur du mode "EASE"
// ALORS j'affiche dans la console "mode facile"
// Sinon j'affiche dans la console "Sélectionnez un mode"

if (mode ===Mode.HARD) {
    console.log("Mode difficile");
} else if (mode === Mode.NORMAL) {
    console.log("Mode normal");
} else if (mode === Mode.EASE) {
    console.log("Mode facile");
} else {
    console.log("Selectionnez un mode");
}

export {};
