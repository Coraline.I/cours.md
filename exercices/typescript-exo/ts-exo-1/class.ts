/*
    Class :
    Petite note : Toutes les propriétés et méthodes sont "public", vous êtes libres de choisir entre "public", "protected" ou "private"
 */

interface IUser {
    firstName: string;
    lastName: string;
    email: string;
    phone?: string;
    present: boolean;
    room: number;

    isPresent(): boolean;

    getFullName(): string;

    call(): void;

    sendEmail(): void;
}
let user: User = new User("")
// La class "User" doit respecter l'interface "IUser",
// Typez la class "User" avec l'interface "IUser"
// Et ajouter les propriétés et les méthodes afin de respecter l'interface "IUser"
class User implements IUser {
    public firstName: string = null;
    public lastName: string = null;
    public email: string = null;
    public phone?: string = null;
    public present: boolean = true;
    public room: number = null;

    constructor(firstName: string, lastName: string, email: string, room: number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.present = present;
        this.room = room;

    }

    public isPresent(): boolean {
        return true;
    }

    public getFullName(): string {
        return ""
    }

    public call(): void {
        return void;
    };

    public sendEmail(): void {
    };
}




interface IAnimal {
    name: string;
    race: string;
    age: number;
    category: number

    isDangerous(): boolean;

    isSleeping(): boolean;

    callVeterinary(): void;

    callOwner(): void;
}

// La class "Animal" doit respecter l'interface "IAnimal",
// Typez la class "Animal" avec l'interface "IAnimal"
// Et ajouter les propriétés et les méthodes afin de respecter l'interface "IAnimal"
class Animal {
}


interface IIdentity {
    age: number;
    name: string;
}

// L'interface "IContact" doit étendre de l'interface "IIdentity",
// modifiez l'interface "Icontact" pour que ce soit le cas
interface IContact {
    phone: string;
    email: string;

    call(number: string): void;
}

// La class "Contact" doit respecter l'interface "IContact",
// Typez la class "Contact" avec l'interface "IContact"
// Et ajouter les propriétés et les méthodes afin de respecter l'interface "IContact"
class Contact {
}


export {};
