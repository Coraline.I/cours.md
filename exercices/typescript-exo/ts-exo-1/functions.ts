/*
    Functions :
    Typez les fonctions suivante (paramètres et retour de fonction) de la bonne manière
    Et corrigez les types qui ne sont pas correctes
 */

function fn1(): number {
    return 23;
}

function fn2(number1: number) {
    return number1 * 34;
}

function fn3(age: number | string): string {
    return 'Mon age est : ' + age;
}

function getFullName(firstName:string, lastName:string): string {
    return firstName + ' ' + lastName;
}

function fn4(arrayOfStudentNames: string[]): void {
    return;
}

// Ici typez "fn5" pour que son type corresponde à la fonction qui lui est assigné juste après
let fn5: (name: string) => boolean; // fonction retourne boolean
fn5 = function (name) {
    return true;
};

// Ici typez "fn6" pour que son type corresponde à la fonction qui lui est assigné juste après
let fn6: (isPresent: boolean) => boolean;
fn6 = function (isPresent) {
    return !isPresent;
};

// Ici typez "fn7" pour que son type corresponde à la fonction qui lui est assigné juste après
let fn7: (nb1: number) => string;
fn7 = function (nb1) {
    return (nb1 * 24) + '';
};

export {};
