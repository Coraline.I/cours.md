/*
    Interfaces :
 */

/*
    1) Créer une interface "IUser" qui a les propriétés :
    - name => est une string
    - age => est un nombre
    - email => est une string
    - connected => est une méthode ne prenant aucun paramètre et retournant un boolean

    2) Déclarer une variable "user" et typer cette variable avec l'interface "IUser"
    3) Assigner à la variable "user" un objet respectant l'interface "IUser"
 */

interface IUser {
    name: string;
    age: number;
    email: string;

    connected(): boolean;
}

let user: IUser;
user = {
    name: "Jean",
    age: 22,
    email: "js@gmail.com",
    connected(): boolean {
        return true;
    }
};

/*
    1) Créer une interface "ICar" qui a les propriétés :
    - model => est une string
    - km => est optionelle et est un nombre
    - klaxon => est une méthode ne prenant aucun paramètre, et qui retourne une string

    2) Déclarer une variable "car" et typer cette variable avec l'interface "ICar"
    3) Assigner à la variable "car" un objet respectant l'interface "ICar"
 */

interface ICar {
    model: string;
    km?: number;
    klaxon(): string;
}
let car: ICar;
car = {
    model: "BMW",
    // km: 12345,
    klaxon(): string {
        return "bip bip";
    }
};

/*
    1) Créer une interface "IContact" qui a les propriétés :
    - address => est une string
    - email => est optionnelle et est une string
    - phone => est une string

    2) Déclarer une variable "contact" et typer cette variable avec l'interface "IContact"
    3) Assigner à la variable "contact" un objet respectant l'interface "IContact"
 */

interface IContact {
    address: string;
    email?: string;
    phone: string;
}
let contact: IContact;
contact = {
    address: "insert address",
   // email: "js@gmail.com",
    phone: "06"
}

/*
    1) Créer une interface "ISubContact" qui étends de l'interface "IContact" et qui a les propriétés :
    - callTheNumber => est une méthode ne prenant aucun paramètre, qui retourne une string

    2) Déclarer une variable "subContact" et typer cette variable avec l'interface "ISubContact"
    3) Assigner à la variable "subContact" un objet respectant l'interface "ISubContact"
 */

interface ISubContact extends IContact {
    callTheNumber(): string;
}
let subContact: ISubContact;
subContact = {
    callTheNumber(): string {
        return "06";
    },
    address: "insert address",
    // email: "js@gmail.com",
    phone: "06"
};

/*
    1) Créer une interface "IAnimal" qui a les propriétés :
    - race => est une string
    - age => est un nombre
    - vaccins => est un tableau de string
    - sleeping => est un boolean

    2) Déclarer une variable "animal" et typer cette variable avec l'interface "IAnimal"
    3) Assigner à la variable "animal" un objet respectant l'interface "IAnimal"
 */

interface IAnimal {
    race: string;
    age: number;
    vaccins: string[];
    sleeping: boolean;
}
let animal: IAnimal;
animal = {
    race: "cat",
    age: "16",
    vaccins: ["A jour"],
    sleeping: true
};

export {};
