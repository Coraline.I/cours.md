/*
    Types assertions:
    Réalisez les assertions de types
 */

let nb: unknown = 24;
let nb1: number = nb as number; // <-- Réalisez l'assertion de type avec la syntaxe "as"

let userName: unknown = 24;
let fullName: string = <string>userName; // <-- Réalisez l'assertion de type avec la syntaxe entre crochet "<>"

let notes: unknown = [10, 12, 20];
let allNotes: number[] = notes as number[]; // <-- Réalisez l'assertion de type avec la syntaxe de votre choix

let names: unknown = ['Jean', 'Pauline'];
let allNames: string[] = names as string[]; // <-- Réalisez l'assertion de type avec la syntaxe de votre choix


export {};
