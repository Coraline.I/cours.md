/*
    Basic Types :
    Typez les déclarations de variables suivante de la bonne manière
    Et corrigez les types qui ne sont pas correctes
 */

let age: number = 13;

let studentName: string = "Jean";

let isPresent: boolean = true;

let studentNames: string[] = ['Jean', 'Audrey', 'Hadi'];

let notes: number[] = [14, 20, 35, 24];

let notes2: string[] = ["14", "20", "35", "24"];

let presences: boolean[] = [true, false, true, true, false];

let student: {name: string, age: number, present: boolean} = {name: string, age: number, present: boolean};

let students: {name: string, age: number, present: boolean}[] = [
    {name: 'Jean', age: 22, present: true},
    {name: 'Alina', age: 20, present: true},
    {name: 'William', age: 25, present: true},
];

let sentence:string =
    "Hello, my name is " +
    studentName +
    ".\\n\\n" +
    "I'll be " +
    (age + 1) +
    " years old next month.";

let age2: number = 24;

let age3: string = (age + age2) + '';

let age4: string = age + age3;

let array: any[] = [12, "Jean", true, "124"];

let var1: string = "12";

let var2: number = 423;

let var3: { age: number} = {age: 12};

let var4: number[] = [132, 3453, 464, 463];

let var5: string = "true";

export {};

