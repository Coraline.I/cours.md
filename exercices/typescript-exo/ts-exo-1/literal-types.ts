/*
    Literal types :
 */


// Typez la variable "mode"
// La variable "mode" ne peut contenir comme valeur qu'une string étant parmis les strings suivantes : "ease", "normal", "hard"
let mode: "ease" | "normal" | "hard";

mode = "hard"

// Typez la variable "type"
// La variable "type" ne peut contenir comme valeur qu'un nombre étant parmis les nombres suivants : 1, 2, 3
let type: 1, | 2 | 3;

type = 3;

export {};
