// const age = prompt('Quel est votre age?');
//
// alert(age);
//
// console.log(age);

//Exercice 1 :
//Déclare une variable nommé "age" contenant le nombre 24
//Déclare une constante nommé "name" contenant le nom (la chaîne de charactère) de ton choix */

let age = 24;
const name = "Co";

// Exercice 2 : Affiche dans la console de ton navigateur les variables "age" et "name"

console.log('Exercice 2 :', age, name);

//  Exercice 3 : Multiplie la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 72

let add = age * 3;
console.log('Exercice 3 :', add);

//ou

console.log(age * 3);

//  Exercice 4 : Soustrais la variable "age" avec "age" et affiche le résultat dans la console
//  Résultat souhaité : 0

let subs = age - age;
console.log('Exercice 4 :', subs);

// ou

console.log(age - age);

//  Exercice 5 : Divise la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 8

let quotient = age / 3;
console.log('Exrercice 5 :', quotient);

// ou

console.log(age / 3);

/*
    Exercice 6 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" moins 3
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 21

age -= 3; // pour éviter d'écrire age = age - 3, on renvoit le - devant le =.
console.log('Exercice 4 :', age);

/*
    Exercice 7 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" multiplié par 10
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 210

age *= 10;
console.log('Exercice 7 :', age);

/*
    Exercice 8 :
    - Déclare une variable "age2" et assigne la valeur 50
    - Puis affiche la variable "age2" dans la console
*/

let age2 = 50;
console.log('Exercice 8 :', age2);

/*
    Exercice 9 :
    - Additionne "age" avec "age2"
    - Puis affiche le resultat de l'addition dans la console
*/
//  Résultat souhaité : 260

let twoAges = age + age2;
console.log('Exercice 9 :', twoAges);

// ou
console.log(age + age2);

// C'est un exemple pour montrer la marche à suivre sur les prochains exercices.
// Exercice Exemple : Affiche dans la console le résultat de : "age" est-il inférieur à "age2" ?
// Réponse exemple : console.log ('Exercice exemple :', age < age2);

// Exercice 10 : Affiche dans la console le résultat de : "age" est-il supérieur à "age2" ?

console.log('Exercice 10 :', age > age2);

// Exercice 11 : Affiche dans la console le résultat de : "age2" est-il inférieur à "age" ?

console.log('Exercice 11 :', age2 < age);

// Exercice 12 : Affiche dans la console le résultat de : "age2" est-il supérieur ou égale à 50 ?

console.log('Exercice 12 :', age2 >= 50);

// Exercice 13 : Affiche dans la console le résultat de : "age" est-il inférieur ou égale à 10 ?

console.log('Exercice 13 :', age <= 10);

// Exercice 14 : Affiche dans la console le résultat de : "age" est-il égale à 210 ?

console.log('Exercice 14 :', age === 210);

// Exercice 15 : Affiche dans la console le résultat de : "age" est-il strictement égale en valeur et en type à 210 ?

console.log('Exercice 15 :', age === 210);
console.log( age === "210");

/*
 Exercice 16 - Opérateurs logiques : Écris la phrase suivante et affiche dans la console le résultat.
 Indice : Il vous faut utiliser les opérateurs logiques.
 "
     la variable ex16var1 doit être strictement égale à 215 ET
     la variable ex16var2 doit être vrai ET
     la variable ex16var3 doit être strictement égale à la string "blog".
 "
 Résultat souhaité : true
 */
let ex16var1 = 215;
let ex16var2 = true; // la valeur est déjà true donc pas besoin de la tester avec === true dans la console.
let ex16var3 = "blog";

console.log('Exercice 16 ', ex16var1 === 215 && ex16var2 && ex16var3 === "blog");

/*

 Exercice 17 - Opérateurs logiques : Écris la phrase suivante et affiche dans la console le résultat.
 Indice : Il vous faut utiliser les opérateurs logiques.
 "
     (la variable ex17var1 multiplié par 2 et diviser par 5 doit être strictement égale à 8 ET
     la variable ex17var2 doit être non égale simple du nombre 5) OU
     la variable ex17var3 doit être égale à faux
 "
 Résultat souhaité : true
 */
let ex17var1 = 20;
let ex17var2 = "5";
let ex17var3 = false;

console.log('Exercice : 17', ((ex17var1 * 2 / 5) === 8 && ex17var2 != 5) || ex17var3 === false);

// Exercice 18 : Déclare une variable de ton choix sans lui affecter de valeur et affiche le résultat dans la console.

let var1;
console.log(var1);

// Exercice 19 : Affiche dans la console le type de toutes les variables déclarées précédemment.

console.log('Exercice 19 :', typeof name, typeof age, typeof var1);



/*
    Bonus 1 :
    1. Déclarer une variable nommée "bonus" et par défaut assignez-lui le nombre 2
    2. En utilisant un opérateur artihmétique, déterminez si la valeur contenue dans la variable "bonus"
       est paire ou impaire
    3. Assignez le nombre 3 à la variable "bonus"
    4. Comme pour le point 2, affichez dans la console si la variable "bonus" est paire ou impaire

    Dans le point 2 et 4, la façon qui nous permet de déterminer
    si un nombre est paire ou impaire en utilisant un certain opérateur
    est identique.
    Résultat souhaité :
    - Pour le point 2 => 0
    - Pout le point 4 => 1

    Expliquez en quelques lignes ce que représente 0 et 1
    ce que cela traduit de la valeur contenue dans la variable "bonus"
 */
 let bonus = 2;

console.log('Bonus 1 :', bonus % 2) // pair car renvoie la valeur 0

bonus = 3;

console.log(bonus % 3); // impaire car renvoit la valeur 1


/*
    Bonus 2 :
    Complétez le bonus 1, en y ajoutant une condition qui affiche dans la console :
    - "Nombre paire" si la valeur contenue dans la variable "bonus" est paire
    - "Nombre impaire" si la valeur contenue dans la variable "bonus" est impaire
 */

if (bonus % 2) {
    console.log(bonus, 'impaire');
} else {
    console.log(bonus, 'pair');
}