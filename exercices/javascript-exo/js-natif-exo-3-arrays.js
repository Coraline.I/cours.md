/*
    Les tableaux :
    Découvrez comment manipuler des chaînes de caractères.
    Vous trouverez dans la documentation des méthodes utiles
    pour réaliser les objectifs décrits après.
    Prenez votre temps pour lire la description et comprendre l'utilisation des méthodes
    et n'hésitez pas à vous entraider.

    Lorsque je vous demande d'afficher une donnée, il faut l'afficher dans la console.

    Conseil : n'hésitez pas à tester en ajoutant ou modifiant la valeur des variables.

    Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 */

/**
 * Exercice 1 :
 * Créez un tableau vide dans une variable ou constante
 */

let arr = [];
console.log('Ex 1 :', arr);

/**
 * Exercice 2
 * Créez un tableau dans une constante avec des valeurs initiales : "orange", "red", "pink", "blue"
 */

const arr2 = ["orange", "red", "pink", "blue"];
console.log('Ex 2 :', arr2);
/**
 * Exercice 3
 * Créez un tableau dans une variable avec un maximum de 10 entrées et remplie du boolean "false"
 */

arr = new Array(10).fill(false);
console.log('Ex 3 :', arr);

/**
 * Exercice 4
 * Créez un tableau dans une variable et ajoutez-y plusieurs valeurs de votre choix
 */

let ex4Arr = ["Je", "suis", 1, "fille", true];
console.log('Ex 4 :', ex4Arr);

/**
 * Exercice 5
 * Créez un tableau dans une variable et affichez la deuxième valeur dans la console
 */

console.log('Ex 5:', ex4Arr[1]);

/**
 * Exercice 6
 * Créez un tableau dans une variable, ajoutez-y 10 éléments de votre choix et supprimez la deuxième valeur
 */

let newArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
newArr.splice(1, 1);
console.log('Ex 6 :', newArr);


/**
 * Exercice 7 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la première valeur
 */
/**/

newArr.shift();
console.log('Ex 7:', newArr);

// ou newArr.splice(0,1);

/** * Exercice 8 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la dernière valeur
 */

newArr.pop();
console.log('Ex 8:', newArr);


/**
 * Exercice 9 :
 * Créez un tableau dans une variable nommée "mat" contenant 2 tableaux vide
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 0
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 1
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 2
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 3
 * Affichez le résultat dans la console
 */

// let mat = [[0, 3], [1, 2]];
let mat = [[], []];

mat[0].push(0);
mat[1].push(1);
mat[1].push(2);
mat[0].push(3);
console.log('Ex 9:', mat);

/**
 * Exercice 10 : Bonus "Immutabilité"
 * Recherchez et décrivez ce qu'est "l'immutabilité"
 * Pourquoi il faut faire attention à l'immutabilité lorsqu'on manipule des tableaux
 */

// C'est lorsque qu'on attribue la même valeur d'un tableau, à un autre tableau.

let arr11 = arr10;

// Si on souhaite supprimer une valeur dans le tableau d'origine, la valeur copié va forcément changer.
// Il va donc falloir procéder autrement.

let arr11 = [...arr10]; // utiliser la fonction spread pour que la valeur reste la même dans le cas où la valeur arr11 doit être changée.



