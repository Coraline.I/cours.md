// exercice 1 : créer une fonction qui prend en paramètre un nombre entier et qui
// affiche tous les entiers entre 0 et lui même


function maFn (number){
    let arr = [];
    for (let i = 0; i <= number; i++){
        arr.push(i);
    }
    return console.log(arr);
}

maFn(5);

console.log('[FOR]Exo1 : ',);

// exercice 2 : créer une fonction qui prend en paramètre 2 nombres entiers et qui
// affiche tous les entiers entre le premier et le second

console.log('[FOR]Exo2 : ', '');

// exercice 3 : créer une fonction qui génère un tirage de loto avec
// 7 nombres compris entre 1 et 45

console.log('[FOR]Exo3 : ', '');

// exercice 4 : générer un tableau contenant des nombres pairs consécutifs,
// le premier nombre du tableau doit être 4,
// on doit arrêter de remplir le tableau quand il y a 20 nombres pairs dans le tableau

console.log('[FOR]Exo4 : ', '');

// exercice 5 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires

// const person = {firstName: '', lastName: '', age: 0};
// console.log(person);

// bonus : calculer la moyenne des âges de personnes en utilisant un reduce

console.log('[FOR]Exo5 : ', '');


// exercice 6 : Écrire un programme qui affiche les nombres de 1 à 199 avec
// un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.

console.log('[FOR]Exo6 : ', '');
