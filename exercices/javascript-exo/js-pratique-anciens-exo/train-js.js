class User {
    name;
    age;
    presence;
    dancing;
    rapping;

    constructor(name, age, presence, dancing, rapping) {
        this.name = name;
        this.age = age;
        this.presence = true;
        this.dancing = true;
        this.rapping = true;
    }

    isPresent() {
        return this.presence;
    }

    isDancing() {
        return this.dancing;
    }

    isRapping() {
        return this.rapping;
    }
}

class User3 extends User {

    constructor(name, age) {
        super(name, 26);
    }
}

class User4 extends User {
    constructor(name) {
        super(name);
    }
}

let user = new User("Ayo", 28, false, false, true);
let user2 = new User("Jim", 26, true, true, false);

let user3 = new User3("Koo", 24);
let user4 = new User4("Coraline");

user.isRapping();
user2.isDancing();