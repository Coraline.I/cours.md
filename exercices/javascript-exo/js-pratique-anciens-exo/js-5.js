/*
    Exercice 1 :
    Lorsque nous manipulions les strings, nous avons vu comment accéder à un caractère
    Déclarer une boucle for commençant à "let i = 0" et dont la limite est la longueur de la string "str"
    Dans la boucle, grâce à la variable "i" accéder et affichez chaque caractère de la string "str" dans la console

    **** Compter le nombre de voyelles et autres caractères de la string "str" ****

    À chaque itération de la boucle déterminez si le caractère de la string "str"
    auquel on accède en utilisant la variable "i" est compris dans la tableau "arr".
    C'est-à-dire est-ce que le caractère est un voyelle ?
    SI le caractère est une voyelle ALORS incrémentez la variable "vowels"
    SINON incrémentez la variable "others"
    Attention ! Assurez-vous de compter chaque caractère qu'il soit en minuscule ou en majuscule.

    À la suite et à l'extérieur de la boucle, affichez dans la console :
        - le nombre de caractères que possède la string "str
        - le nombre de voyelles comptées
        - le nombre d'autres caractères comptées
 */
// La variable str ne comporte aucun nombre
let str = "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.";
let arr = ['a', 'e', 'i', 'o', 'u', 'y']; // Tableau contenant les voyelles
let vowels = 0;
let others = 0;

// Ici déclarez votre boucle for

for (let i = 0; i < str.length; i++) {
   if (arr.indexOf(str[i].toLowerCase())) {
       vowels++;
   } else {
       others++;
   }
}
/*
    À la suite et à l'extérieur de la boucle, affichez dans la console :
        - le nombre de caractères que possède la string "str
        - le nombre de voyelles comptées
        - le nombre d'autres caractères comptées
 */

console.log(str.length, vowels, others);
/*
    Ajoutez une condition :
    Si la variable "vowels" est strictement différent du nombre 220
    de la string "str" Alors affichez dans une alerte en utilisant la fonction "alert()"
    le message "Le compte n'est pas bon ! Corrigez votre code."
    Il n'y a pas de SINON dans cette condition
    Remarque : le nombre 220, est le nombre de voyelles compté pour la string "str" de base
    Si vous modifiez la string "str" il se peut que le nombre de voyelles ne soit plus égale à 220.
    Donc attention.
 */

if (vowels !== 220) {
    alert("Le compte n'est pas bon ! Corrigez votre code.");
}
