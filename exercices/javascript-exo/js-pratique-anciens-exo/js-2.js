/*
    Manipuler les strings :
    Découvrez comment manipuler des chaînes de caractères.
    Vous trouverez dans la documentation des méthodes utiles
    pour réaliser les objectifs décrits après.
    Prenez votre temps pour lire la description et compendre l'utilisation des méthodes
    et n'hésitez pas à vous entraider.

    Conseil : n'hésitez pas à tester en ajoutant ou modifiant la valeur des variables.

    Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String
 */


let str;

// Corrigez les strings suivantes en échappant les bons caractères.
str = 'C\'est Cécile qui l\'a souhaité, Elle m\'a dit qu\'elle aurait préféré venir.';
str = '/!\\Attention/!\\ Ce message est important /!\\Attention/!\\';
str = "Elle m'a dit \"À demain\", après m'avoir saluer et dit \"Bonjour\" !";

// Concaténez en rajoutant une virgule (", ") la variable "str" avec la constante "text1" et affichez le résultat dans la console
// Résultat souhaité : "Il y a Sam, Jean, Olivier et Tom"
str = "Il y a Sam";
const text1 = "Jean, Olivier et Tom";
console.log(str + ", " + text1);

// Concaténez la variable "str" avec la constante "age" et affichez le résultat dans la console
// Résultat souhaité : "Son âge : 22"
str = "Son âge : ";
const age = 22;
console.log(str + age);

// Concaténez la variable "str", avec la constante "price" multiplié par 4 et la string " €"
// Affichez le résultat dans la console
// Résultat souhaité : "Son prix : 800 €"
str = "Son prix : ";
const price = 200;
console.log(str + (price * 4) + "€");

// Concaténez la variable "str", avec la constante "name1", la string " et " et la contante "name2"
// Affichez le résultat dans la console
// Résultat souhaité : "Il y a Sam et Steve"
str = "Il y a ";
const name1 = "Sam";
const name2 = "Steve";

console.log(str + name1 + " et " + name2);

/*
    Mettre en majuscule la variable "str" et affichez la string transformée dans la console
    Résultat souhaité : "TEXTE EN MAJUSCULE"
 */
str = 'texte en majuscule';

console.log(str.toUpperCase());

/*
    Mettre en minuscule la variable "str" et affichez la string transformée dans la console
    Résultat souhaité : "text en minuscule"
 */
str = 'TEXTE EN MINUSCULE';

console.log(str.toLowerCase());

/*
    Déterminez en utilisant une méthode
    si la variable "str" contient la string contenue dans la variable "search"
    Et affichez le résultat dans la console
    Résultat souhaité : true
 */
str = 'Il est allé se balader';
let search = 'se balader';

console.log(str.includes(search));

/*
    Déterminez en utilisant une méthode
    si la variable "str" commence par la string contenue dans la constante "start"
    Et affichez le résultat dans la console
    Résultat souhaité : false
 */
str = 'Bonjour Jean !';
const start = "Bonjour, !";

console.log(str.startsWith(start));

/*
    Déterminez en utilisant une méthode
    La position de la string "La" dans la string contenue dans la variable "str"
    Et affichez le résultat dans la console
    Résultat souhaité : 0
 */
str = 'La position de la string dans une autre';

console.log(str.indexOf("La"));

/*
    Récupérez la longueur de la string contenue dans la variable "str"
    Et affichez le résultat dans la console
    Résultat souhaité : 24
 */
str = 'La longueur de ma string';

console.log(str.length);

/*
    En utilisant une méthode
    Remplacez toutes les occurences de la string "a"
    Dans la string contenue dans la variable "str"
    Par la string "o"
    Et affichez le résultat dans la console
    Résultat souhaité : "Mo bonone o plonté"
 */
str = 'Ma banane a planté';

console.log(str.replaceAll('a', 'o'));

/*
    En utilisant une méthode
    Séparez grâce à un espace (" ") tous les mots contenues dans la variable "str"
    Et affichez le résultat dans la console
    Résultat souhaité :
    Un tableau de string => ['Une', 'phrase', 'complexe', 'avec', 'des', 'mots']
 */
str = 'Une phrase complexe avec des mots';

console.log(str.split(" "))

/*
    En utilisant une méthode
    Effacez les espaces en début et fin de la string contenue dans la variable "str"
    Et affichez le résultat dans la console
    Résultat souhaité : "Mon text avec trop d'espace"
 */
str = '        Mon text avec trop d\'espace        ';

console.log(str.trim());

