
/*
    Répondre ICI (dans le commentaire)
    à toutes les questions en numérotant et espaçant vos réponses.
*/

/*
    Exercice 1 :
    Déclare une variable nommé "age" contenant le nombre 24
    Déclare une constante nommé "name" contenant le nom (la chaîne de charactère) de ton choix
*/

let age = 24;
const name = "Tom";

// Exercice 2 : Affiche dans la console de ton navigateur les variables "age" et "name"

console.log('EX 2 :', age, name);

//  Exercice 3 : Multiplie la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 72

let add = age * 3;
console.log('EX 3 :', add);

//  Exercice 4 : Soustrais la variable "age" avec "age" et affiche le résultat dans la console
//  Résultat souhaité : 0

let sub = age - age;
console.log('EX 4 :', sub);

//  Exercice 5 : Divise la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 8

let divi = age / 3;
console.log('EX 5 :', divi);

/*
    Exercice 6 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" moins 3
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 21

age -= 3;
console.log('EX 6 :', age);

/*
    Exercice 7 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" multiplié par 10
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 210

age *=10;
console.log('EX 7 :', age);

/*
    Exercice 8 :
    - Déclare une variable "age2" et assigne la valeur 50
    - Puis affiche la variable "age2" dans la console
*/

let age2 = 50;
console.log('EX 8 :', age2);

/*
    Exercice 9 :
    - Additionne "age" avec "age2"
    - Puis affiche le resultat de l'addition dans la console
*/
//  Résultat souhaité : 260

console.log('EX 9 :', age + age2);

// C'est un exemple pour montrer la marche à suivre sur les prochains exercices.
// Exercice Exemple : Affiche dans la console le résultat de : "age" est-il inférieur à "age2" ?
// Réponsé éxemple : console.log ('Exercice exemple :', age < age2);

// Exercice 10 : Affiche dans la console le résultat de : "age" est-il supérieur à "age2" ?

console.log('EX 10 :', age > age2);

// Exercice 11 : Affiche dans la console le résultat de : "age2" est-il inférieur à "age" ?

console.log('EX 11 :', age2 < age);

// Exercice 12 : Affiche dans la console le résultat de : "age2" est-il supérieur ou égale à 50 ?

console.log('EX 12 :', age2 >= 50);

// Exercice 13 : Affiche dans la console le résultat de : "age" est-il inférieur ou égale à 10 ?

console.log('EX 13 :', age <= 10);

// Exercice 14 : Affiche dans la console le résultat de : "age" est-il égale à 210 ?

console.log('EX 14 :', age == 210);

// Exercice 15 : Affiche dans la console le résultat de : "age" est-il strictement égale en valeur et en type à 210 ?

console.log('EX 15 :', age === 210 && age === typeof 210);

/*
 Exercice 16 - Opérateurs logiques : Écris la phrase suivante et affiche dans la console le résultat.
 Indice : Il vous faut utiliser les opérateurs logiques.
 "
     la variable ex16var1 doit être strictement égale à 215 ET
     la variable ex16var2 doit être vrai ET
     la variable ex16var3 doit être strictement égale à la string "blog".
 "
 Résultat souhaité : true
 */
let ex16var1 = 215;
let ex16var2 = true;
let ex16var3 = "blog";

console.log('EX 16 :', ex16var1 === 215 && ex16var2 === true && ex16var3 === "blog");

/*
 Exercice 17 - Opérateurs logiques : Écris la phrase suivante et affiche dans la console le résultat.
 Indice : Il vous faut utiliser les opérateurs logiques.
 "
     (la variable ex17var1 multiplié par 2 et diviser par 5 doit être strictement égale à 8 ET
     la variable ex17var2 doit être non égale simple du nombre 5) OU
     la variable ex17var3 doit être égale à faux
 "
 Résultat souhaité : true
 */
let ex17var1 = 20;
let ex17var2 = "5";
let ex17var3 = false;

console.log('EX 17 :', (ex17var1 * 2 && ex17var1 / 5 === 8 && ex17var3 !== 5) || ex17var3 === false);

/*
    Bonus 1 :
    1. Déclarer une variable nommée "bonus" et par défaut assignez-lui le nombre 2
    2. En utilisant un opérateur artihmétique, déterminez si la valeur contenue dans la variable "bonus"
       est paire ou impaire
    3. Assignez le nombre 3 à la variable "bonus"
    4. Comme pour le point 2, affichez dans la console si la variable "bonus" est paire ou impaire

    Dans le point 2 et 4, la façon qui nous permet de déterminer
    si un nombre est paire ou impaire en utilisant un certain opérateur
    est identique.
    Résultat souhaité :
    - Pour le point 2 => 0
    - Pout le point 4 => 1

    Expliquez en quelques lignes ce que représente 0 et 1
    ce que cela traduit de la valeur contenue dans la variable "bonus"
 */

let bonus = 2;

console.log('BONUS 1 :', bonus % 2); //pair

bonus = 3;

console.log('BONUS 1-2 :', bonus % 2); // impair

/*
    Bonus 2 :
    Complétez le bonus 1, en y ajoutant une condition qui affiche dans la console :
    - "Nombre paire" si la valeur contenue dans la variable "bonus" est paire
    - "Nombre impaire" si la valeur contenue dans la variable "bonus" est impaire
 */

if (bonus % 2) {
    console.log('EX BONUS 2 :', "PAIR");
} else {
    console.log('BONUS 2-2 :', "IMPAIR");
}