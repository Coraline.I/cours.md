/**
 * Exercice 1 :
 * Utilisez la fonction prompt() pour demander à l'utilisateur son âge
 * Sauvegarder dans une variable nommée "age" la valeur entrée par l'utilisateur
 * Transcrire la phrase suivante en code javascript valide :
 *      SI la variable "age" est supérieur ou égale à 18
 *          ALORS j'affiche dans la console la string "Majeur"
 *      SINON j'affiche dans la console la string "Mineur"
 */

// prompt("Vous avez quel âge?");

let age;

if (age >= 18) {
    console.log('EX 1 :', "Majeur");
} else {
    console.log('EX 1 :', "Mineur");
}

/**
 * Exercice 2 :
 * Déclarez une variable nommée "hour" contenant le nombre de votre choix entre 0 et 24
 * Transcrire la phrase suivante en code javascript valide :
 *      SI la variable "hour" est un nombre entre 0 (inclus) et 8 (exclus)
 *          ALORS j'affiche dans la console la string "Bon réveil"
 *      SINON SI la variable "hour" est un nombre entre 8 (inclus) et 17 (exclus)
 *          ALORS j'affiche dans la console la string "Bonjour"
 *      SINON SI la variable "hour" est un nombre entre 17 (inclus) et 22 (exclus)
 *          ALORS j'affiche dans la console la string "Bonsoir"
 *      SINON SI la variable "hour" est un nombre entre 22 (inclus) et 0 (exclus)
 *          ALORS j'affiche dans la console la string "Bonne nuit"
 *      SINON
 *          ALORS j'affiche dans la console le contenu de la variable "hour"
 *
 * Remarque : Faites attention à vos conditions et respecter les annotations "inclus" et "exclus".
 */

let hour = 23;

if (hour >= 0 && hour < 8) {
    console.log('EX 2 :', "Bon réveil");
} else if (hour >= 8 && hour < 17) {
    console.log('EX 2 :', "Bonjour");
} else if (hour >= 17 && hour < 22) {
    console.log('EX 2 :', "Bonsoir");
} else {
    console.log('EX 2 :', "Bonne nuit");
}

/**
 * Exercice 3 :
 * Déclarez une variable nommée "text" et assignez lui la valeur "Boujour, Jean !"
 * SI la variable "name" possède une valeur différente de null, undefined ou une string vide
 * SINON assignez à la variable "text" la valeur "Bonjour !"
 */


let text;
if (name !== null && name !== undefined && name !== "") {
    text = "Bonjour, Jean!";
} else {
    text = "Bonjour!";
}
console.log('EX 3 :', text);

/**
 * Exercice 4 :
 * Réalisez l'exercice 3 avec l'opérateur ternaire cette fois.
 */

(name !== null && name !== undefined && name !== "") ? "Bonjour, Jean!" : "Bonjour!";
console.log('EX 4 :', text);

/**
 * Exercice 5 :
 * En utilisant une boucle for, affichez une suite de nombre allant de 0 à 30 avec l’information pair ou impaire pour chaque nombre.
 */

for (let i = 0; i < 30; i++) {
    let odd;
    if (i % 2) {
       odd = "Impair";
    } else {
        odd = "Pair";
    }
    console.log('EX 5 :', i, odd);
}

/**
 * Exercice 6 :
 * Déclarez une variable i égale à 0 par défault;
 *      SI la variable i est égale ou supérieur à 0 et est inférieur à 30
 *          ALORS affichez Premier mois,
 *      SINON SI i est supérieur à 30 et inférieur à 60
 *          ALORS affichez Deuxième mois,
 *       SINON affichez Reste de l’année.
 */

let i = 0;

if (i >= 0 && i < 30) {
    console.log('EX 6 :', "Premier mois");
} else if (i > 30 && i < 60) {
    console.log('EX 6 :', "Deuxième mois");
} else {
    console.log('EX 6 :', "Reste de l'année");
}

/**
 * Exercice 7 :
 * Même que exercice 5, mais utiliser le modulo pour savoir si le nombre est pair ou impaire, ainsi que du ternaire.
 */

for (let i = 0; i <= 30; i++) {
    const odd = i % 2 ? "Impaire" : "Pair";
    console.log('EX 7 :', i, odd);
}

/**
 * Exercice 8 :
 * Déclarez une variable nommée "arr" contenant un tableau vide
 * En utilisant une boucle for remplisser le tableau de nombre allant de 0 à 20 (inclus)
 */

let arr = [];

for (let i = 0; i <= 20; i++) {
    arr.push(i);
}
console.log('EX 8 :', arr);

/**
 * Exercice 9 :
 * Déclarez une variable nommée "arr2" contenant le tableau [13, 29, 46, 52]
 * Déclarer une variable nommée "arr3" contenant un tableau vide
 * Remplissez "arr3" avec les valeurs du tableau "arr2" multiplié par 2
 * Résultat souhaité :
 * SI la variable "arr2" = [4, 9, 12]
 * La variable "arr3" doit être égale à [8, 18, 24]
 * Chaque nombre du tableau "arr2" doit être multiplié par 2 et sauvegardé dans le tableau "arr3"
 */

let arr2 = [13, 29, 46, 52];
let arr3 = [];

for (let i = 0; i < arr2.length; i++) {
    arr3.push(arr2[i] * 2);
}
console.log('EX 9 :', arr2, arr3);
/**
 * Exercice 10 :
 * Déclarez une variable nommée "arr4" contenant le tableau [12, 42, 63, 56, 76, 934, 2394, 340, 4812]
 * En utilisant une boucle fort affichez chaque valeur du tableau "arr4" dans la console
 */

let arr4 = [12, 42, 63, 56, 76, 934, 2394, 340, 4812];

for (let i = 0; i < arr4.length; i++) {
    console.log('EX 10 :', arr4[i]);
}

/**
 * Exercice BONUS :
 * Mettez à jour l'exercice 1
 * Il faut vérifier la donnée entrée par l'utilisateur
 * Tant que la variable "age" n'est pas un nombre
 * Utilisez la fonction prompt() pour demander à l'utilisateur son âge
 *
 * Mettez ensuite à jour la condition :
 *      SI la variable "age" est supérieur ou strictement égale à 18
 * Avec cette mise à jour de la condition, le code ne fonctionnera pas.
 * Faites en sorte que le script fonctionne en rajoutant une étape.
 *
 */

