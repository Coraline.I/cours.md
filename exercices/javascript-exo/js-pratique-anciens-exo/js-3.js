/*
    Les tableaux :
    Découvrez comment manipuler des tableaux.
    Vous trouverez dans la documentation des méthodes utiles
    pour réaliser les objectifs décrits après.
    Prenez votre temps pour lire la description et compendre l'utilisation des méthodes
    et n'hésitez pas à vous entraider.

    Lorsque je vous demande d'afficher une donnée, il faut l'afficher dans la console.

    Conseil : n'hésitez pas à tester en ajoutant ou modifiant la valeur des variables.

    Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 */

/**
 * Exercice 1 :
 * Créez un tableau vide dans une variable ou constante
 */

let arr = [];
console.log('EX 1 :', arr);

/**
 * Exercice 2
 * Créez un tableau dans une constante avec des valeurs initiales : "orange", "red", "pink", "blue"
 */

const arr2 = ["orange", "red", "pink", "blue"];
console.log('EX 2 :', arr2);

/**
 * Exercice 3
 * Créez un tableau dans une variable avec un maximum de 10 entrées et remplie du boolean "false"
 */

let arr3 = new Array(10).fill(false);
console.log('EX 3 :', arr3);

/**
 * Exercice 4
 * Créez un tableau dans une variable et ajoutez-y plusieurs valeurs de votre choix
 */

let arr4 = ["Pratiquer", "est", "la", "clé"];
console.log('EX : 4', arr4);

/**
 * Exercice 5
 * Créez un tableau dans une variable et affichez la deuxième valeur dans la console
 */

let arr5 = [1, 2];
console.log('EX 5 :', arr5[1]);

/**
 * Exercice 6
 * Créez un tableau dans une variable, ajoutez-y 10 éléments de votre choix et supprimez la deuxième valeur
 */

let arr6 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
arr6.splice(1, 1)
console.log('EX 6 :', arr6);

/**
 * Exercice 7 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la première valeur
 */

let arr7 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
arr7.shift();
console.log('EX 7 :', arr7);

/**
 * Exercice 8 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la dernière valeur
 */

let arr8 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
arr8.splice(arr8.length - 1);
console.log('EX 8 :', arr8);

/**
 * Exercice 9 :
 * Créez un tableau dans une variable nommée "mat" contenant 2 tableaux vide
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 0
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 1
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 2
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 3
 * Affichez le résultat dans la console
 */

let mat = [[], []];
mat[0].push(0);
mat[1].push(1);
mat[1].push(2);
mat[0].push(3);
console.log('EX 9 :', mat);

/**
 * Exercice 10 : Bonus "Immutabilité"
 * Recherchez et décrivez ce qu'est "l'immutabilité"
 * Pourquoi il faut faire attention à l'immutabilité lorsqu'on manipule des tableaux
 */

// VOIR SPREAD OPERATOR