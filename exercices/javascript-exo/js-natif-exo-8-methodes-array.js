// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array

/**
 * Utilisez les méthodes natives aux tableaux, vous les retrouverez sur le lien du dessus.
 * Lorsque le nom, la valeur n'est pas défini, c'est à vous de décider.
 *
 * La fonction "getRandomArray" retourne un tableau de nombre aléatoire de la longueur défini par le paramètre "limit"
 * Lorsque vous invoquez la fonction si vous ne donnez pas la limite, par défaut elle sera de 30, le tableau comportera 30 valeurs.
 */

function getRandomArray(limit = 30) {
    let arr = [];
    for (let i = 0; i < limit; ++i) {
        arr.push(Math.floor(Math.random() * 10));
    }
    return arr;
}

/**
 * Exercice 1 :
 * Créez un tableau avec la fonction getRandomArray() et multipliez par deux chaque valeur du tableau
 * Explication : Ce sera la même logique pour "Créez un tableau avec la fonction getRandomArray"
 * 1. Déclarer une variable ou une constante
 * 2. Assignez à la variable ou la constante créée le retour de la fonction "getRandomArray" avec le paramètre de votre choix
 * Exemple : let exemple = getRandomArray(10);
 */

let arr = getRandomArray(10);
let arrMultipled = arr.map(function (value) {
    return value * 2
});
console.log('Ex 1:', arr, arrMultipled);



/**
 * Exercice 2 :
 * Créez un tableau avec la fonction getRandomArray() et triez par ordre croissant les valeurs
 * Utilisez une méthode pour cette exercice
 */

let arr2 = getRandomArray(10);
let arr2Sorted = [...arr2];
arr2Sorted.sort();

console.log('Ex 2:', arr2, arr2Sorted);


/**
 * Exercicec 3.1 :
 * Créez un tableau avec la fonction getRandomArray() et additionnez toutes les valeurs entre elles
 * Utilisez une méthode pour cette exercice
 */

let arr3 = getRandomArray(10);
let somme3 = arr3.reduce(function (accumulateur, valeur) {
    console.log('Ex 3-1 :', accumulateur, valeur);

   return accumulateur + valeur;

}, 0);
console.log('3 :', somme3);

/**
 * Exercice 3.2 :
 * Créez un tableau avec la fonction getRandomArray() et remplacez chaque valeur par cette phrase
 * "Pair : VALUE" si la valeur est pair
 * "Impaire : VALUE" si la valeur est impair
 * "VALUE" doit être remplacé par le nombre testé
 * Utilisez une méthode pour cette exercice
 */

let arr32 = getRandomArray(10);
let arr32Mapped = arr32.map(function (value) {

   /* if (value % 2) {
        return "Impair : " + value;
    } else {
        return "Pair : " + value
    }
*/
    // ternaire
    return (value % 2) ? "Impair : " + value : "Pair : " + value;
});
console.log('Ex32 :', arr32Mapped);
/**
 * Exercice 4 :
 * Créez un tableau avec la fonction getRandomArray() et filtrez seulement les valeurs pair
 * Utilisez une méthode pour cette exercice
 */

let arr4 = getRandomArray(10);
let arr4Filtered = arr4.filter(function (value) {
     return !(value % 2);
});
console.log('Ex 4 :', arr4Filtered);

/**
 * Exercice 5 :
 * Créez un tableau avec la fonction getRandomArray() et filtrez seulement les valeurs impair
 * Utilisez une méthode pour cette exercice
 */

let arr5 = getRandomArray(10);
let arr5Filtered = arr5.filter(function (value) {
    return value % 2;
});
console.log('Ex 5 :', arr5Filtered);

/**
 * Exercice 6 :
 * Créez un tableau avec la fonction getRandomArray() et
 * Déterminez si au moins une des valeurs du tableau créée est supérieur ou égale à 5
 * Utilisez une méthode pour cette exercice
 */

let arr6 = getRandomArray(10);
let arr6Sommed = arr6.some(function (value) {
    return value >= 5;

});
console.log('Ex 6 :', arr6Sommed);


/**
 * Exercice 7 :
 * Créez un tableau avec la fonction getRandomArray() et
 * Déterminez si toutes les valeurs du tableau créé sont inférieur ou égale à 5
 * Utilisez une méthode pour cette exercice
 */

let arr7 = getRandomArray(10);
let arr7Everied = arr7.every(function (value) {
    return value <= 5;
});
console.log('Ex 7 :', arr7Everied);

/**
 * Exercice 8 :
 * Déclarez une fonction nommé "createArray" qui prend en paramètre :
 * - "length" => un nombre
 * - "value" => une valeur
 *
 * La fonction "createArray" doit retourner un nouveau tableau dont la longueur
 * correspond au nombre passer en premier paramètre et rempli par la valeur passé en deuxième paramètre
 * Exemple : J'invoque la fonction "createArray" avec le nombre 3 en premier paramètre et le boolean true en second paramètre
 * La valeur retourné par la fonction doit être un tableau avec 3 true => [true, true, true]
 */


function createArray(length, value) {

    let arr8 = [];
    for (let i = 0; i < length; i++) {
        arr8.push(value);
    }
    return arr8;
}
arr8 = createArray(3, true);
console.log('Ex 8:', arr8);


/**
 * Exercice 9 :
 * Affectez par décomposition la première valeur du tableau "arr9" à la variable "a",
 * la seconde valeur du tableau "arr9" à la variable "b" et
 * toutes les valeurs restantes à la variable "rest"
 */
let a, b, rest;
let arr9 = [1, 2, 3, 4, 5, 6, 7, 8];

[a, b, ...rest] = arr9;
console.log('Ex 9:', a);
console.log('Ex 9:', b);
console.log('Ex 9:', rest);

/**
 * Exercice 10 :
 * Déclarez un objet nommé "user" possédant la propriété "name", "email", "connected", "age"
 */

let user = {name: "Coraline", email:"js@gmail.com", connected:true, age:18};
console.log('Ex 10:', user);

/**
 * Exercice 10-2 :
 * Affichez dans la console l'age de l'objet "user" seulement si il est majeur
 */

if (user.age >= 18) {
    console.log('Ex 10-2 :', user.age);
}


/**
 * Exercice 11 :
 * Récupérez grâce à la décomposition les propriétés "name", "email" et "age" de l'objet "user"
 * Renommez la propriété "name" en "userName"
 * et affichez les variables "userName", "email", "age" dans la console.
 *
 * Remarque: Prenez des initiatives.
 */

let {name: userName, email, age} = user;
console.log('Ex : 11', userName, email, age);

/**
 * Exercice 12 :
 * Créer un nouveau tableau à partir du tableau "arr12"
 * Remarque : Attention il y a un point important à respecter
 * Utilisez le "spread operator"
 */

let arr12 = [1, 2, 4];
arr122 = [...arr12];

console.log('Ex 12:', arr12, arr122);

/**
 * Exercice 13 :
 * Créer un nouveau tableau nommé "arrCopied" à partir du tableau "arr13"
 * Remarque : Attention il y a un point important à respecter
 * Utilisez le "spread operator"
 */
let arr13 = ['Valeur', 'à', 'copier', 'en', 'faisant', 'attention', 'à', 'un', 'certain', 'principe'];
arrCopied = [...arr13];
console.log('Ex : 13', arrCopied);

/**
 * Exercice 14 :
 * Créez une string en REJOIGNANT chaque valeur du tableau "arrCopied" par un espace " "
 * Affichez la phrase créée dans la fonction.
 * Pour cet exercice utilisez une méthode uniquement.
 * Résultat souhaité dans la console : 'Valeur à copier en faisant attention à un certain principe'
 */

// let str = [...arrCopied];
// console.log('Ex 14 :', str.join(" "));

console.log('Ex 14 :', arrCopied.join(" "));
/**
 * Exercice 14 :
 * Créer un nouvel objet nommé "objCopied" à partir de l'objet "obj14"
 * Remarque : Attention il y a un point important à respecter
 * Utilisez le "spread operator"
 */

let obj14 = {model: 'Tesla', owner: 'Sam'};
let objCopied = {...obj14};
console.log('Ex 14 ;', objCopied);

/**
 * Exercice 15 :
 * Modifiez l'objet "objCopied" en y ajoutant la propriété "date" (valeur de votre choix)
 * Affichez dans la console "obj14" et "objCopied" seul l'objet "objCopied" doit avoir
 * la propriété "date" si l'ojbet "obj14" l'a également c'est que vous avez mal copié l'objet "obj14"
 * dans l'exercice 14
 *
 * Bonus : Modifiez également le tableau "arrCopied" en le modifiant, puis affichez "arr13" et "arrCopied"
 * si la modification d'un des deux tableaux est visible sur les deux, vous avez mal copié le tableau "arr13"
 * dans l'exercice 13
 */

objCopied.date = "17-01-2021";
console.log('Ex 15 :', obj14, objCopied);

/**
 *  Exercice 16 : PARTIE 1
 *  Déclarez une fonction qui prend en argument un tableau de prénoms (tableau de string) nommé "names"
 *  Chaque prénom compris dans le tableau "names" (l'argument de la fonction)
 *  doit être utilisez pour créer un objet qui aura la propriété "name" àgale au prénom.
 *  Votre fonction doit retourner un tableau d'objets.
 *
 *  Résultat souhaité :
 *  J'invoque la fonction en lui donnant en paramètre le tableau ["Jean", "Steve"]
 *  La fonction doit me retourner un tableau d'objets => [{name: "Jean"}, {name: "Steve"}]
 *  Chaque prénom (string) doit être utilisé et "transformé" en objet avec la propriété "name"
 */

function createUsers(names) {
 /*   let arr = [];
*/
/*    for (let i = 0; i < names.length; i++) {
        let obj = {name: names[i]};
        arr.push(obj);
    }*/

  return names.map(function (value) {
        return {name: value};
    });

}
let users = createUsers(["Jean", "Steve"]);
console.log('Ex 16-1 :', users);

/**
 *  Exercice 16 : PARTIE 2
 *  Stockez le tableau d'objets obtenu dans la partie 1 de l'exercice 16 dans une variable nommée "users"
 *  En utilisant une méthode de tableau,
 *  Ajoutez dans chaque objet la propriété "age"
 *  avec un nombre entier aléatoire compris entre 0 et 10
 *
 *  Résultat souhaité : En reprenant l'exemple précédant
 *  [{name: "Jean", age: 5}, {name: "Steve", age: 10}]
 */

let usersAged = users.map(function (user) {
    let obj = {...user};
    obj.age = Math.floor(Math.random() * 10);
    console.log(obj);
    return obj;
});
    console.log('Ex 162 :', usersAged);

/**
 *  Exercice 16 : PARTIE 3
 *  Implémentez cette phrase
 *  "Affichez dans la console seulement les utilisateurs ayant plus de 5 ans"
 *
 *  Utilisez une méthode de tableau
 */
let usersAgedFiltered = usersAged.filter(function (obj) {
    return obj.age > 5;
});

console.log('163 :', usersAgedFiltered);
/**
 *  Exercice 16 : PARTIE 4
 *  Trier par ordre decroissant le tableau "users"
 *  en fonction de l'âge des utilisateurs contenus dans le tableau "users"
 *  Stoquez le tableau trié dans une constante nommée "usersSorted"
 *
 *  Utilisez une méthode de tableau
 */

const usersSorted = usersAged.sort(function (a, b) {
    return b.age - a.age;
});
console.log('164 :', usersAged);

/**
 *  Exercice 16 : PARTIE 5
 *  Accédez à l'utilisateur (l'objet) le plus jeune du tableau "usersSorted"
 *  Implémentez la phrase en dessous de manière dynamique
 *  "Affichez dans la console le nom de l'utilisateur le plus jeune du tableau "usersSorted""
 */

let youngUser = usersSorted[usersSorted.length - 1];
console.log(youngUser.name + " est le plus jeune.");