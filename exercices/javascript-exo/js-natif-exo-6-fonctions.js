/**
 * Nommez les fonctions et les arguments comme vous le souhaitez
 * Lorsqu'ils ne sont pas spécifiés dans l'énoncé.
 * Pareil pour le type des arguments.
 *
 * Si aucune donnée de retour n'est indiqué dans l'énoncé la fonction ne doit pas retourner de donnée.
 */

/**
 * Exercice 1 :
 * Définissez une fonction nommée "maFn" qui prend en arguments :
 *  - "name" : une string représentant un nom
 *  - "age" : un nombre
 *
 *  Cette fonction ne retourne aucune donnée. Elle affiche dans la console une phrase
 *  "Bonjour {{NAME}}, vous avez {{AGE}} ans."
 *  Exemple :
 *  Si j'invoque la fonction avec comme premier paramètre "Samuel" et deuxième paramètre 22.
 *  Dans la console il doit y avoir d'affiché la phrase "Bonjour Samuel, vous avez 22 ans."
 *
 *  Invoquez cette fonction 3 fois avec des paramètres différents
 */

function maFn(name, age) {
    console.log('Exo 1 :', "Bonjour" + name + "," + " Vous avez " + age + " ans.");
    // ou
    console.log(`Bonjour ${name}, vous avez ${age} ans.`);
}
maFn(" Coraline", 29);
maFn(" Jean", 22);
maFn(" Sam", 27);



/**
 * Exercice 2 :
 * Définissez une fonction nommée "maFn2" avec un argument nommé "arg"
 * qui affiche l’argument dans la console et ne retourne aucune donnée.
 */
function maFn2(arg) {
    console.log('Exo 2 :', arg);
}
maFn2("bye");


/**
 * Exercice 3 :
 * Définissez une fonction nommée "multiplyBy" qui prend un nombre en argument et
 * qui le multiplie par deux. Cette fonction retourne le résultat de la multiplication.
 * Appelez cette fonction 2 fois avec des paramètres différents.
 */

function multiplyBy(nb) {
    return nb * 2;

}
    multiplyBy(17);
    multiplyBy(19);

/**
 * Exercice 4 :
 * Définissez une fonction nommée "evenOrOdd" qui prend en argument un nombre
 * La fonction doit retourner la string "Pair" si le nombre passé en argument est un nombre pair
 * La fonction doit retourner la string "Impair" si le nombre passé en argument est un nombre impair
 *
 * Remarque : Une fonction peut contenir plusieurs fois le mot clé "return".
 * Par contre, dès que le mot-clé "return" est rencontré par le moteur d'execution, la fonction va s'arrêter.
 * Les instructions des lignes suivantes ne seront pas exécutées.
 */

function evenOrOdd(nb) {
    if (nb % 2) {
        return "impair";
    } else {
        return "pair";
    }

}
evenOrOdd(10);


/**
 * Exercice 5 :
 * Définissez une fonction sans arguments qui s’invoque elle-même
 * Pour trouver la solution, recherchez sur internet.
 */

(function () {
    console.log('Ex 5 :');

})();

/**
 * Exercice 6 :
 * Définissez une fonction qui prend un nombre en argument et qui s’invoque elle-même en passant un nombre en argument
 * Pour trouver la solution, recherchez sur internet.
 */

(function (nb) {
    console.log('Ex 6 :', nb);
})(6);

/**
 * Exercice 7 :
 * Déclarez une fonction sans définir les arguments (Une fonction qui ne prend aucun argument)
 * Invoquez cette fonction 1 fois en donnant 10 paramètres (arguments)
 * Affichez la liste des arguments dans la console et grâce à une boucle for affichez chaque argument dans la console.
 */

function maFn7() {
    console.log('Ex 7 :', arguments);
    for (let i = 0; i < arguments.length; i++) {
        console.log(i, arguments[i]);
    }
}
maFn7(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

/**
 * Exercice 8 :
 * Déclarez une fonction nommée "createArray"
 * qui prend en premier argument un nombre nommée "length" et en deuxième argument nommée "value" (une valeur dont peu importe le type)
 * Cette fonction "createArray" doit créer un tableau de la longueur définie par l'argument 'length' et le remplir totalement par la valeur de "value"
 * Elle retourne le tableau créé et rempli.
 * Invoquez cette fonction 2 fois avec les paramètres de votre choix et affichez les deux résultats dans la console.
 *
 * Résultat souhaité :
 * La fonction "createArray" invoquée avec comme premier paramètre le nombre 3 et en second paramètre le boolean true
 * La fonction doit me retourner un tableau d'une longueur de 3 contenant 3 boolean true => [true, true , true]
 * J'affiche le retour de la fonction "createArray" dans la console.
 */

function createArray(length, value) {
/*    let arr = [];

    for (let i = 0; i < length; i++) {
        arr.push(value);
    }*/

    return new Array(length).fill(value);

}
console.log(createArray(3, true));

// méthode plus courte



/**
 * Exercice 9 :
 * Déclarez une fonction nommée "copyArrayAndMultiplyByTwo" qui prend en argument le tableau de nombre "arr9"
 * Cette fonction doit retourner un nouveau tableau contenant chaque valeur du tableau passé en paramètre ("arr9") multiplié par 2
 * Affichez le tableau retourné par la fonction "copyArrayAndMultiplyByTwo" dans la console avec le tableau initial "arr9"
 *
 * Résultat souhaité :
 * En invoquant "copyArrayAndMultiplyByTwo" avec le tableau "arr9"
 * la fonction doit retourné un nouveau tableau => [6, 68, 1868, 20, 118]
 *
 * Astuce : La solution se trouve dans l'une des vidéos
 */
let arr9 = [3, 34, 934, 10, 59];

function copyArrayAndMultiplyByTwo(arr) {
    let newArr = [];

    for (let i = 0; i < arr.length; i++) {
        newArr.push(arr[i] * 2);
    }
    return newArr;
}
console.log(copyArrayAndMultiplyByTwo(arr9));

/**
 * Exercice 10 : Bonus
 * Reproduire la suite de fibonacci : 1-1-2-3-5-8-13-21
 */
