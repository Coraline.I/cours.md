/*
    Exercice :

    Dans le champ "Le nom de la tâche" entrez le nom d'une tâche que vous souhaitez ajouter
    Lorsqu'on clique sur le bouton "Ajouter"
    Créer un nouvel élément "div" qui a la class "task" et le text de l'élément créé correspond à celui entré dans l'input
    lorsqu'on clique sur l'élément créé je veux qu'il soit supprimé du tableau et du html
    Insérer l'élément dans la div qui a l'id "tasks"

     Ajouter les tâches présentent dans le tableau "tasks" dans la div qui a l'id "tasks" comme précédement

     Lorsqu'on clique sur le bouton "Enregistrer", je veux que vous m'affichiez toutes les tâches qui n'ont pas été supprimé
 */

let tasks = [
    'Sortir le chien',
    'Nettoyer le salon',
    'Jetter les poubelles'
];

window.addEventListener("DOMContentLoaded", function () {
    console.log("DOM CHARGÉ");

    const btnTask = document.getElementById("btn");
    const greenContainer = document.getElementById("tasks");
    const newInput = document.getElementById("input");
    const btnCook = document.getElementById("btn-cook");


    btnTask.addEventListener("click", function () {
        if (newInput.value !== '') {
            let newTask = document.createElement('div');
            greenContainer.appendChild(newTask);

            newTask.innerText = newInput.value;
            newTask.classList.add('task');

        }

        btnCook.addEventListener("click", function () {
            // Supprimer du html
            greenContainer.removeChild(this);
        });

        // Afficher éléments restants
       let tasksRestantes = greenContainer.getElementsByClassName("task");

       for (let i = 0; i < tasksRestantes.length; i++) {
           console.log(tasksRestantes[i].innerText);
        }
    });

});

// C'est pas bon.......